..  _gallery:
.. raw:: html

    <video autoplay loop poster="_static/gallery_static.png">
        <source src="_static/gallery.mp4" type="video/mp4">
        <source src="_static/gallery.webm" type="video/webm">
        Sorry, your browser doesn't support HTML5 video.
    </video>


Gallery
**************************

This section contains a gallery of visual representations of pyGDM simulations. Clicking on the image leads to a corresponding example or tutorial page where the shown results are reproduced.


|
|


.. image:: _static/gallery/incident_field.png
   :scale: 50 %
   :target: tutorials/06_visualize_incident_fields.html
   
|
|


.. image:: _static/gallery/vectorbeams.png
   :scale: 50 %
   :target: examples/example15_vectorbeams.html
   
|
|

.. image:: _static/gallery/mie_theory_dielectric.png
   :scale: 40 %
   :target: examples/example01_mie01.html
   
|
|

.. image:: _static/gallery/spectra_gold_rod_polarization.png
   :scale: 50 %
   :target: tutorials/02_spectra.html

|
|

.. image:: _static/gallery/nearfield_maps.png
   :scale: 50 %
   :target: tutorials/03_field_maps.html

|
|

.. image:: _static/gallery/multimaterial_graded_index.png
   :scale: 75 %
   :target: tutorials/08_multi_materials_structures.html

|
|

.. image:: _static/gallery/polarization_conversion.png
   :scale: 50 %
   :target: examples/example06_polarization_conversion.html

|
|

.. image:: _static/gallery/decay_rate_electric_magnetic.png
   :scale: 30 %
   :target: examples/example08_decay_rate.html

|
|

.. image:: _static/gallery/directional_dipole_emitter_splitring.png
   :scale: 40 %
   :target: examples/example05_splitring_with_dipole.html

|
|

.. image:: _static/gallery/plasmonic_YagiUda.png
   :scale: 90 %
   :target: examples/example13_plasmonic_YagiUda_directional_antenna.html

|
|

.. image:: _static/gallery/multipole_decomposition.png
   :scale: 100 %
   :target: examples/multipole/exampleMultipole_ex2_disc.html

|
| 

.. image:: _static/gallery/heat_generation.png
   :scale: 40 %
   :target: examples/example07_heat_generation.html

|
|

.. image:: _static/gallery/chirality.png
   :scale: 80 %
   :target: examples/chirality/example10c_optical_chirality_3.html

|
|

.. image:: _static/gallery/optical_forces.png
   :scale: 60 %
   :target: examples/chirality/example11_optical_forces.html
   
|
|

.. image:: _static/gallery/fast_electrons.png
   :scale: 50 %
   :target: examples/electrons/exampleFastElec_fast_electrons_ex2.html

|
|

.. image:: _static/gallery/rasterscans_tpl_heat.png
   :scale: 30 %
   :target: tutorials/04_rasterscans_tpl_heat.html

|
|

.. image:: _static/gallery/rasterscans_ldos.png
   :scale: 30 %
   :target: tutorials/05_rasterscans_ldos.html

|
|

.. image:: _static/gallery/retarded_propagator.png
   :scale: 70 %
   :target: tutorials/exampleRetard_goldsphere_on_substrate.html

..    :align: left


